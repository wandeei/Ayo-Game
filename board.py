# -*- coding: utf-8 -*-
"""
Created on Sat May 13 07:02:30 2017

@author: Dell
"""

class Board:
    def __init__(self):
        self.board = []
        
        
    def initialize_board(self):
        self.board = [[4 for col in range(6)] for row in range(2)]
        self.display_board()
        
        
    def display_board(self, player1_wins = 0, player2_wins = 0):
        print("\n")
        for i in self.board:
            print(i)
        print("Player 1 has won: ", player1_wins, " seeds")
        print("Player 2 has won: ", player2_wins, " seeds")
        
    def hole_is_playable(self, row_index, column_index):
        
        if(self.board[row_index][column_index] != 0): #it contains seeds
            return True
        else:
            return False
        
        
    def make_move(self, row_index, column_index, player):
        
        seeds = self.pack_seeds(row_index, column_index)
        
        while(seeds != 0):
            
            if((row_index == 1) & (column_index == 5)):
                row_index = 0
                column_index = 5
                self.board[row_index][column_index] += 1
                
            elif((row_index == 0) & (column_index == 0)):
                row_index = 1
                column_index = 0
                self.board[row_index][column_index] += 1
                
            else:
                if(row_index == 1):
                    column_index += 1
                    self.board[row_index][column_index] += 1
                else:
                    column_index -= 1
                    self.board[row_index][column_index] += 1
                
            seeds = seeds - 1
        
        self.capture_seeds(row_index, column_index, player)
        
        
    def pack_seeds(self, row_index, column_index):
        
        seeds = self.board[row_index][column_index]
        self.board[row_index][column_index] = 0;
        
        return seeds
        
    def seeds_left_on_board(self):
        seeds = 0
        for i in range(2):
            for j in range(6):
                seeds += self.board[i][j]
        
        return seeds
        
        
    def capture_seeds(self, row_index, column_index, player):
                
        if(player.player_index != row_index):
            
            while(True):
                if((self.board[row_index][column_index] == 2) or (self.board[row_index][column_index] == 3)):
                    player.won_seeds += self.pack_seeds(row_index, column_index);
                else:
                    break
                
                if((row_index == 1) & (column_index != 0)):
                    column_index -= 1
                
                elif((row_index == 0) & (column_index != 5)):
                    column_index += 1
                
                else:
                    break
    
    def end_game_conditions(self, player1, player2):
        if((player1.won_seeds >= 25) or (player2.won_seeds >= 25)):
            return False
        elif(self.seeds_left_on_board() <= 2):
            return False
        else: 
            return True

        
        


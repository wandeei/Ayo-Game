# -*- coding: utf-8 -*-
"""
Created on Sat May 13 08:33:01 2017

@author: Dell
"""

#NAME: OKELEJI FATAI BABAWANDE
#MATRIC: 120407037
        

from board import Board
from player import Player
import random


def main():
    
    
    print("**********************AYO  OLOPON GAME****************************\n")
    
    print("The holes are indexed from zero to five on each row.\n")
    
    print("for a demo, player 1 plays against random \n")
   
    ayo = Board()
    ayo.initialize_board()
   
   
   #Designate Players
    player1 = Player(0)
    player1.turn = True
    
    player2 = Player(1)
    
    while(ayo.end_game_conditions(player1,player2)):
        
        if(player1.turn == True):
            player2.turn = True
            player1.turn = False
            move = int(input("Player 1's move: "))
            
            if(ayo.hole_is_playable(player1.player_index,move)):
                ayo.make_move(player1.player_index,move,player1)
                ayo.display_board(player1.won_seeds,player2.won_seeds)
            else:
                print("You cannot play that hole, play another hole")
                player2.turn = False
                player1.turn = True
                continue
        
        elif(player2.turn == True):
            player2.turn = False
            player1.turn = True
            #move = int(input("Player 2's move: ")) #uncomment to deactivate random play
            move = random.randint(0, 5) #comment out to deactivate random play
            
            if(ayo.hole_is_playable(player2.player_index,move)):
                ayo.make_move(player2.player_index,move,player2)
                ayo.display_board(player1.won_seeds,player2.won_seeds)
            else:
                print("You cannot play that hole, play another hole")
                player1.turn = False
                player2.turn = True
                continue
            
    if(player1.won_seeds > player2.won_seeds):
        print("***********************PLAYER 1 WON*****************")
    elif(player2.won_seeds > player1.won_seeds):
        print("**********************PLAYER 2 WON****************")
    else: print("********************DRAW*******************")
    
    
    
if __name__ == "__main__":
    main()